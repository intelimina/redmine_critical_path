class IssueTree
  unloadable

  attr_accessor :current_project
  attr_accessor :forest_cache
  attr_accessor :critical_path

  def self.get_issue_tree_ids(parent_issue_id)
    # TODO: modify this to use the issue_relations table, and not the issues table
    # see parent_issue_ids for notes
    
    Issue.where(root_id: parent_issue_id).pluck(:id)
  end

  def self.start_column
    # Some issues why we're not using easy_start_date_time anymore, in favor of start_date
    # e.g. see issue #32303
    # @@start_column ||= Issue.first.respond_to?(:easy_start_date_time) ? :easy_start_date_time : :start_date
    :start_date
  end

  def self.due_column
    # @@due_column   ||= Issue.first.respond_to?(:easy_due_date_time) ? :easy_due_date_time : :due_date
    :due_date
  end

  def self.compute_total_time(issue_ids)
    # TODO: modify this to use the sum of durations of all the issues
    # NOT the difference of the earliest start date and latest due date
    sql  = ActiveRecord::Base.send( :sanitize_sql, ["SELECT TIMESTAMPDIFF(SECOND, MIN(%s), MAX(%s)) FROM issues ", start_column, due_column], "issues")
    sql += ActiveRecord::Base.send( :sanitize_sql_array, ["WHERE id in (?)", issue_ids])

    Issue.connection.execute(sql).first[0] || 0
  end

  # Return all root nodes
  def parent_issue_ids
    # TODO: modify this to find the parent_issue_ids from issue_relations where relation_type = 'precedes'
    # I guess a parent issue id is an issue that is in issue_from_id and not in issue_to_id

    Issue.where(project_id: current_project.id).where("parent_id IS NULL").pluck(:id)
  end

  def get_heaviest_path
    max_weight = 0
    longest_path = nil

    parent_issue_ids.each do |parent_id|
      new_path, weight = measure_forrest_weight(parent_id)
      if max_weight < weight
        longest_path = new_path
        max_weight = weight
      elsif max_weight == weight
        longest_path += new_path
      end
    end
    return longest_path, max_weight
  end

  def measure_forrest_weight(node)
    arr = []
    arr_weight = 0

    downlines(node).each do |dl|
      temp_arr, temp_weight = measure_forrest_weight(dl)
      
      if temp_weight > arr_weight
        arr = temp_arr
        arr_weight = temp_weight
      end

      if temp_weight == arr_weight
        arr += temp_arr
      end
    end

    arr << node
    arr_weight += compute_weight(node) #1 #TIMEDIFF(node)

    return arr.uniq, arr_weight
  end

  def downlines(node)
    relations = IssueRelation.where(relation_type: "precedes").where(issue_from_id: node)
    direct_desendant_ids = relations.pluck(:issue_to_id)
  end

  def compute_weight(issue_id)
    sql  = ActiveRecord::Base.send( :sanitize_sql, ["SELECT TIMESTAMPDIFF(SECOND, start_date, due_date) FROM issues "], "issues")
    sql += ActiveRecord::Base.send( :sanitize_sql_array, ["WHERE id in (?)", issue_id])

    result = Issue.connection.execute(sql).first[0]
    p result
    return result || 0
  end

  def all_trees
    @forest_cache ||= parent_issue_ids.collect do |parent_id|
      [ parent_id, IssueTree.compute_total_time(IssueTree.get_issue_tree_ids(parent_id)) ]
    end.sort_by { |k,v| v }
  end

  def critical_path_id
    root_id, value = all_trees.last
    root_id
  end

  def critical_path_issue_ids
    # @critical_path ||= IssueTree.get_issue_tree_ids(critical_path_id)
    path, weight = get_heaviest_path
    @critical_path = path
  end

  def in_critical_path?(issue)
    critical_path_issue_ids.include?(issue.id)
  end
end