class IssueTreesController < ApplicationController
  unloadable

  before_filter :find_project, :authorize

  def index

  end

  def critical_path
    tree = IssueTree.new
    tree.current_project = @project
    @critical_path = tree.critical_path_issue_ids
    render json: @critical_path
  end

  private
  def find_project
    @project = Project.find(params[:project_id])
  end
end
