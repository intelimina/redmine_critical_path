# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

get 'projects/:project_id/issue_trees', to: 'issue_trees#index', as: 'issue_trees'
get 'projects/:project_id/critical_path', to: 'issue_trees#critical_path', as: 'critical_path'