require 'redmine'

Redmine::Plugin.register :redmine_critical_path do
  name 'Critical Path Plugin'
  author 'Rystraum Gamonez'
  description 'Plugin to display critical path'
  version '0.0.1'
  url 'https://bitbucket.org/intelimina/redmine_critical_path'
  author_url 'http://intelimina.com/wp/'

  project_module :critical_path do
    permission :view_critical_path, issue_trees: [:index, :critical_path]
  end

end